$(function () {
    $('.slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider'
    });
    $('.slider-nav').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.slider',
        nextArrow: '<button type="button" class="item-next">Next</button>',
        prevArrow: '<button type="button" class="item-prev">Prev</button>',
        dots: false,
        focusOnSelect: true,
        vertical: true
    });

    $('.slider-product').slick({
        slidesToShow: 4,
        slidesToScroll: 4,
        nextArrow: '<button type="button" class="product-item-next">Next</button>',
        prevArrow: '<button type="button" class="product-item-prev">Prev</button>',
        dots: true
    });

    $(".footer__payment-systems-logo img")
        .mouseover(function(){
            var src = $(this).attr("src");
            var new_src = src.replace(".png","-hover.png");
            $(this).attr("src",new_src);
        })
        .mouseout(function(){
            var src = jQuery(this).attr("src");
            var new_src = src.replace("-hover.png",".png");
            $(this).attr("src",new_src);
        });

    // jQuery(function ($) {
    //     $('input[type=number]').iLightInputNumber();
    // });

});